#!/usr/bin/env bash

echo "Auto Automoney Backend App";

image_name="fl-automoney-backeend"
author="saintent"
docker_image=${author}/${image_name}
startup_dir="startup/"
compose_file="${startup_dir}docker-compose.yml"

PS3='Please enter action: '
options=("Build" "Push" "Start" "StartDevEnv" "Down" "StopDevEnv" "Bash" "RUN BASH" "CONTAINER_PRUNE" "IMAGE_PRUNE" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Build")
            case $1 in
            '-t' | "--tag")
                shift
                tag_name=$1
                echo "Tag : $tag_name";
                docker build -t ${docker_image}:${tag_name} -f  Docker/Dockerfile .
                break;
                ;;
                *)
                echo "latest";
                docker build -t ${docker_image} -f Docker/Dockerfile .
                break;
                ;;
            esac
            break;
            ;;
        "Push")
            echo "Push app saintent/erp-backend"
            case $1 in
            '-t' | "--tag")
                shift
                tag_name=$1
                echo "Tag : $tag_name";
                docker push ${docker_image}:$tag_name
                break;
                ;;
                *)
                echo "latest";
                docker push ${docker_image}
                break;
                ;;
            esac

            break;
            ;;
        "Quit")
            break
            ;;
        "Start")
            docker-compose -p fl_automoney_backeend -f startup/docker-compose.yml --env startup/.env up -d
            break
            ;;
        "StartDevEnv")
            docker-compose -p fl_automoney_backeend_dev_env  --project-directory ./Docker -f startup/docker-compose-dev.yml up -d
            break
            ;;
        "Down")
            docker-compose -p fl_automoney_backeend -f startup/docker-compose.yml --env startup/.env down
            break
            ;;
        "StopDevEnv")
            docker-compose -p fl_automoney_backeend_dev_env  --project-directory ./Docker -f startup/docker-compose-dev.yml down
            break
            ;;
        "Bash")
#            docker-compose -f startup/docker-compose.yml exec django bash
            break
            ;;
        "RUN BASH")
#            docker-compose -f startup/docker-compose.yml run django bash
            break
            ;;
        "CONTAINER_PRUNE")
            docker container prune
            break
            ;;
        "IMAGE_PRUNE")
            docker image prune
            break
            ;;
        *) echo "invalid option $REPLY"
            break
            ;;
    esac
done
