#!/bin/bash

# Exit immediately if a command exits with a non-zero status.
# http://stackoverflow.com/questions/19622198/what-does-set-e-mean-in-a-bash-script
set -e

# Check if the required PostgreSQL environment variables are set
# Used by docker-entrypoint.sh to start the dev server
#[ -z "$DB_NAME" ] && echo "ERROR: Need to set DB_NAME" && exit 1;
#[ -z "$DB_USERNAME" ] && echo "ERROR: Need to set DB_USERNAME" && exit 1;
#[ -z "$DB_PASSWORD" ] && echo "ERROR: Need to set DB_PASSWORD" && exit 1;
#[ -z "$DB_HOST" ] && echo "ERROR: Need to set DB_HOST" && exit 1;
#[ -z "$DB_PORT" ] && echo "ERROR: Need to set DB_PORT" && exit 1;


# Define help message
show_help() {
    echo """
Usage: docker run <imagename> COMMAND

Commands
bash     : Start a bash shell
start     : Start a application
"""
}

#setup_env() {
##    printenv;
##    sed -i "s/(REDIS_HOST)/${REDIS_HOST}/" /etc/supervisor/supervisord.conf
##}

# Run
case "$1" in
    bash)
        /bin/bash "${@:2}"
    ;;
    start)
        echo "Start node server"
        npm run build
        node .
    ;;
    *)
        show_help
    ;;
esac
