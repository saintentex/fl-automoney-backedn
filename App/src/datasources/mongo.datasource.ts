import {
  inject,
  lifeCycleObserver,
  LifeCycleObserver,
  ValueOrPromise,
} from '@loopback/core';
import {juggler} from '@loopback/repository';
import config from './mongo.datasource.config.json';


@lifeCycleObserver('datasource')
export class MongoDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'mongo';

  constructor(
    @inject('datasources.config.mongo', {optional: true})
    dsConfig: object = config,
  ) {
    if (process.env.DOCKER) {
      const runMongoConfig = {
        ...config,
        host: process.env?.DB_HOST || config.host,
        port: process.env.DB_PORT ? parseInt(process.env.DB_PORT) : config.port,
        user: process.env?.DB_USERNAME || config.user,
        password: process.env?.DB_PASSWORD || config.password,
        database: process.env?.DB_NAME || config.database,
      };
      super(runMongoConfig);
    } else {
      super(dsConfig);
    }
  }

  /**
   * Start the datasource when application is started
   */
  start(): ValueOrPromise<void> {
    // Add your logic here to be invoked when the application is started
  }

  /**
   * Disconnect the datasource when application is stopped. This allows the
   * application to be shut down gracefully.
   */
  stop(): ValueOrPromise<void> {
    return super.disconnect();
  }
}
