// Framework import
import {BootMixin} from '@loopback/boot';
import {
    ApplicationConfig,
    BindingKey,
    createBindingFromClass,
} from '@loopback/core';
import {
    RestExplorerBindings,
    RestExplorerComponent,
} from '@loopback/rest-explorer';
import {RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {CrudRestComponent} from '@loopback/rest-crud';
import {AuthenticationComponent} from '@loopback/authentication';
import {AuthorizationComponent} from '@loopback/authorization';
import {ServiceMixin} from '@loopback/service-proxy';
// Other import
import path from 'path';
import multer from 'multer';
import fs from 'fs';
import _ from 'lodash';
// Application domain
// Sequence
import {MySequence} from './sequence';
// Model
import {User} from './models';
// Service
import {BcryptHasher} from './services/hash.password.bcryptjs';
import {JWTService} from './services/jwt-service';
import {MyUserService} from './services/user-service';
// Other
import {JWTAuthenticationStrategy} from './authentication-strategies/jwt-strategy';
// Binding keys
import {
    PasswordHasherBindings,
    TokenServiceBindings,
    TokenServiceConstants,
    UserServiceBindings,
    FILE_UPLOAD_SERVICE,
    STORAGE_DIRECTORY
} from './keys';


export class IcommerzBackendApplication extends BootMixin(
    ServiceMixin(RepositoryMixin(RestApplication)),
) {
    constructor(options: ApplicationConfig = {}) {
        super(options);
        // Set up binding key
        this.setUpBindings();

        // Set up the custom sequence
        this.sequence(MySequence);

        // Set up default home page
        this.static('/', path.join(__dirname, '../public'));

        // Customize @loopback/rest-explorer configuration here
        this.configure(RestExplorerBindings.COMPONENT).to({
            path: '/explorer',
        });

        // All component
        this.component(CrudRestComponent);
        this.component(RestExplorerComponent);
        // Bind authentication component related elements
        this.component(AuthorizationComponent);
        this.component(AuthenticationComponent);
        // authentication
        this.add(createBindingFromClass(JWTAuthenticationStrategy));

        // Configure file upload with multer options
        this.configureFileUpload(options.fileStorageDirectory);

        this.projectRoot = __dirname;
        // Customize @loopback/boot Booter Conventions here
        this.bootOptions = {
            controllers: {
                // Customize ControllerBooter Conventions here
                dirs: ['controllers'],
                extensions: ['.controller.js'],
                nested: true,
            },
        };
    }

    /**
     * Configure `multer` options for file upload
     */
    protected configureFileUpload(destination?: string) {
        // Upload files to `dist/.sandbox` by default
        destination = destination ?? path.join(__dirname, '../.sandbox');
        this.bind(STORAGE_DIRECTORY).to(destination);
        const multerOptions: multer.Options = {
            storage: multer.diskStorage({
                destination,
                // Use the original file name as is
                filename: (req, file, cb) => {
                    cb(null, file.originalname);
                },
            }),
        };
        // Configure the file upload service with multer options
        this.configure(FILE_UPLOAD_SERVICE).to(multerOptions);
    }

    /*
    * Bind package.json to the application context
     */
    setUpBindings(): void {
        // this.bind(PackageKey).to(pkg);

        this.bind(TokenServiceBindings.TOKEN_SECRET).to(
            TokenServiceConstants.TOKEN_SECRET_VALUE,
        );

        this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
            TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE,
        );

        this.bind(TokenServiceBindings.TOKEN_SERVICE).toClass(JWTService);

        // // Bind bcrypt hash services
        this.bind(PasswordHasherBindings.ROUNDS).to(10);
        this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);

        this.bind(UserServiceBindings.USER_SERVICE).toClass(MyUserService);
    }


}
