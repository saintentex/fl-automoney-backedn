// Loopback import
import {Getter, inject} from '@loopback/core';
import {
    DefaultCrudRepository,
    HasOneRepositoryFactory,
    repository
} from '@loopback/repository';
// Data source
import {MongoDataSource} from '../datasources';
// Model
import {User, UserCredentials} from '../models';
// Relation repository
import {UserCredentialsRepository} from "./user-credentials.repository";

export type Credentials = {
    email: string;
    password: string;
};

export class UserRepository extends DefaultCrudRepository<User,
    typeof User.prototype.id> {
    public readonly userCredentials: HasOneRepositoryFactory<UserCredentials, typeof User.prototype.id>;

    constructor(
        @inject('datasources.mongo') dataSource: MongoDataSource,
        @repository.getter('UserCredentialsRepository') protected userCredentialsRepositoryGetter: Getter<UserCredentialsRepository>,
    ) {
        super(User, dataSource);
        this.userCredentials = this.createHasOneRepositoryFactoryFor(
            'userCredentials',
            userCredentialsRepositoryGetter,
        );

        // add this line to register inclusion resolver
        this.registerInclusionResolver('userCredentials', this.userCredentials.inclusionResolver);
    }

    async findCredentials(
        userId: typeof User.prototype.id,
    ): Promise<UserCredentials | undefined> {
        try {
            const query = this.userCredentials(userId);
            const instance =  await query.get({}, {
                strictObjectIDCoercion: true
            });
            console.log(instance);
            return instance;
        } catch (err) {
            console.log(err);
            if (err.code === 'ENTITY_NOT_FOUND') {
                return undefined;
            }
            throw err;
        }
    }
}
