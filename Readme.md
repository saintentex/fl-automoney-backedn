## Read me

## Importance !!
to start up docker container via **docker_script.sh** must create **.env** file.

| PARAM | DESCRIPTION |
|---|---|
|DB_NAME|Database name|
|DB_USERNAME|root user|
|DB_PASSWORD|*password*|
|DB_HOST|*db host*|
|DB_PORT|*db port*|
|DOCKER|1|
